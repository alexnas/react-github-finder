This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# React GitHub Finder

This React application is designed to find GitHub repos by account name. It shows choosen account profile with a list of public repos. You can shortly move to the choosen account profile or to any its repo.

## Uses

- React
- React Router
- Context
- Hooks
- Axios

## Installation and usage

- Be sure that Git and Node.js are installed globally.
- Clone this repo.
- Run `npm install`, all required components will be installed automatically.
- Run `npm start` to start the project.
- Run `npm build` to create a build directory with a production build of the app.
- Run `npm test` to test the app.

## License

This project is licensed under the terms of the MIT license.
